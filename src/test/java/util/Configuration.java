package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class Configuration {
    private Properties prop = new Properties();
    String envUrlPathFirst;
    String envUrlPathSecond;


    public Configuration() {
        envUrlPathFirst = System.getProperty("user.dir") + "\\src\\test\\resources\\environment\\firstsite_env.properties";
        envUrlPathSecond = System.getProperty("user.dir") + "\\src\\test\\resources\\environment\\secondsite_env.properties";
        loadConfigProperties(envUrlPathFirst);
        loadConfigProperties(envUrlPathSecond);
    }


    public String getFirstBaseUrl() {
        return prop.getProperty("firstbase.url");
    }

    public String getSecondBaseUrl() {
        return prop.getProperty("secondbase.url");
    }

    private void loadConfigProperties(String path) {
        try {
            InputStream in = new FileInputStream(path);
            prop.load(new InputStreamReader(in, StandardCharsets.UTF_8));
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

}