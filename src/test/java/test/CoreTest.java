package test;

import base.PageBase;
import org.junit.Assert;
import org.junit.Test;
import page.HomePage;
import page.TechnicalDocumentPage;

public class CoreTest extends PageBase {

    @Test
    public void checkGivenWordInResult() {
        HomePage homePage = new HomePage(driver);

        Assert.assertTrue("Home page is not opened.", homePage.isHomePageOpened());
        Assert.assertTrue("Search area is not opened.", homePage.isSearchAreaOpened());
        homePage.searchGivenWord("happy");
        Assert.assertTrue("Searched word is not found in searched page.", homePage.isSearchedWordExistInResult("happy"));
    }

    @Test
    public void checkGivenDocumentIsDownload() {
        TechnicalDocumentPage technicalDocumentPage = new TechnicalDocumentPage(driver);

        Assert.assertTrue("Page is not opened.", technicalDocumentPage.isPageOpened("Technical documents"));
        Assert.assertTrue("Dropdown catalog is false.", technicalDocumentPage.isProductExistInDropdown("LUNA EUR"));
        Assert.assertTrue("Opened page is not same with this document.", technicalDocumentPage.isOpenedPageSameWithDocument("document/TechDocHandler.aspx"));
        Assert.assertTrue("Downloaded file is not same with this document.", technicalDocumentPage.isTrueDocumentDownloaded("document/TechDocHandler.aspx"));
    }
}