package page;

import base.PageBase;
import base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import java.util.List;

public class HomePage extends TestBase {

    private RemoteWebDriver driver;
    private String url = "https://www.lely.com/en";


    public HomePage(RemoteWebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, PageBase.WAIT_TIME), this);
    }


    @FindBy(xpath = "//a[@class='logo' and @id='top']")
    public WebElement logoLely;
    @FindBy(id = "cookienotice-button-accept")
    public WebElement cookiesPopup;
    @FindBy(xpath = "//div[@class='header-navigation-button']")
    public WebElement searchTopBtn;
    @FindBy(id = "global-search")
    public WebElement searchArea;
    @FindBy(xpath = "//button[@class='button button-tertiary']")
    public WebElement searchAreaBtn;
    @FindBy(xpath = "//span[@class='highlighted']")
    public List<WebElement> searchedWordsInResults;

    @FindBy(css = ".item-description")
    public List<WebElement> searchedResults;
    @FindBy(xpath = "//li[@class='page page-next']")
    public WebElement nextBtn;


    public void openPage() {
        driver.navigate().to(url);
    }

    public boolean isHomePageOpened() {
        openPage();
        waitForElement(driver, logoLely);
        if (logoLely.isDisplayed()) {
            System.out.println("Homepage is opened.");
            return true;
        }
        return false;
    }

    public boolean isSearchAreaOpened() {
        waitUntilElementThenClick(driver, searchTopBtn);
        waitUntilElementThenClick(driver, cookiesPopup);
        waitForElement(driver, searchArea);
        if (searchArea.isDisplayed()) {
            System.out.println("Search area is opened.");
            return true;
        }
        return false;
    }
    public void searchGivenWord(String text) {
        waitUntilElementThenClick(driver, searchArea);
        searchArea.sendKeys(text);
        waitUntilElementThenClick(driver, searchAreaBtn);
        System.out.println(text + " keyword is searching in page.");
    }

    public boolean isSearchedWordExistInResult(String text) {
        for (int i = 0; i < searchedResults.size(); i++) {
            scrollToFindElement(driver, searchedWordsInResults.get(i));
            if (searchedResults.get(i).getText().toLowerCase().contains(text)) {
                if (searchedWordsInResults.get(0).getText().toLowerCase().equals(text)) {
                    System.out.println(text + " keyword is found in " + i + ". paragraph in result page");
                }
            }
            else {
                System.out.println(text + " keyword is not find in " + i + ". paragraph in result page");
                return false;
            }
        }
        return true;
    }

}
