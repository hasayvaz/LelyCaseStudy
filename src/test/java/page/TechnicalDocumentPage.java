package page;

import base.PageBase;
import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.io.File;
import java.util.List;

public class TechnicalDocumentPage extends TestBase {
    private RemoteWebDriver driver;
    private String urlPage = "https://www.lely.com/techdocs/";

    public TechnicalDocumentPage(RemoteWebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, PageBase.WAIT_TIME), this);
    }


    @FindBy(xpath = "//a[@class='logo' and @id='top']")
    public WebElement logoLely;
    @FindBy(id = "cookienotice-button-accept")
    public WebElement cookiesPopup;
    @FindBy(xpath = "//h1[@class='page-header-title']")
    public WebElement technicalDocumentTitle;
    @FindBy(id = "select2-id_q-container")
    public WebElement productNameCombobox;
    @FindBy(xpath = "//input[@class='select2-search__field' and @role='textbox']")
    public WebElement comboboxTextboxArea;
    @FindBy(xpath = "//option[@value='luna/eur/']")
    public WebElement lunaEurOption;
    @FindBy(xpath = "//h3[@class='result-item-title']")
    public List<WebElement> resultItems;
    @FindBy(xpath = "//p[@class='buttons']/a[@href]")
    public List<WebElement> viewThisDocumentBtn;
    @FindBy(id = "items-list")
    public WebElement itemListArea;
    @FindBy(xpath = "//*[@id='url']")
    public List<WebElement> downloadItemText;


    public boolean isPageOpened(String title) {
        openPage(driver, urlPage);
        waitForElement(driver, logoLely);
        waitUntilElementThenClick(driver, cookiesPopup);

        if (logoLely.isDisplayed() && technicalDocumentTitle.getText().equals(title)) {
            System.out.println(title + " page is opened.");
            return true;
        }
        return false;
    }

    public boolean isProductExistInDropdown(String text) {
        waitUntilElementThenClick(driver, productNameCombobox);
        waitUntilElementThenClick(driver, comboboxTextboxArea);
        fillAreaWithGivenText(comboboxTextboxArea, text);
        waitUntilElementThenClick(driver, lunaEurOption);

        for (int i = 0; i < resultItems.size(); i++) {
            int count = i + 1;
            scrollToFindElement(driver, resultItems.get(i));

            if (resultItems.get(i).getText().toLowerCase().contains(text.toLowerCase())) {
                System.out.println(text + " is seen " + count + ". catalog product.");
            }
            else {
                System.out.println(text + " is not seen " + count + ". catalog product.");
                return false;
            }
        }
        return true;
    }

    public boolean isOpenedPageSameWithDocument(String firstText) {
        waitUntilElementThenClick(driver, viewThisDocumentBtn.get(1));
        switchCurrentPage(driver, 1);
        wait(3);

        if (driver.getCurrentUrl().contains(firstText)){
            System.out.println("Opened page is same with this document.");
            return true;
        }
        return true;
    }

    public boolean isTrueDocumentDownloaded(String text) {
        switchCurrentPage(driver, 0);
        scrollToFindElement(driver, itemListArea);
        waitUntilElementThenClick(driver, viewThisDocumentBtn.get(0));
        wait(5);
        driver.navigate().to("chrome://downloads/");
        waitForElement(driver, downloadItemText.get(0));
        return downloadItemText.get(0).getText().contains(text);
    }


}