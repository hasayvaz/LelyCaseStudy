package base;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import page.HomePage;
import page.TechnicalDocumentPage;
import util.Configuration;
import java.util.concurrent.TimeUnit;

public class PageBase {
    public RemoteWebDriver driver;
    public static String browser;
    public static int WAIT_TIME =15;
    public static int IMPLICITYLY_WAIT =5;
    private static int PAGE_LOAD_TIMEOUT=30;

    @Before
    public void initialize() throws Exception {
        browser = System.getProperty("browser");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--ignore-certifcate-errors");
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--start-maximized");
        chromeOptions.addArguments("--disable-plugins");
        chromeOptions.addArguments("--disable-plugins-discovery");
        chromeOptions.addArguments("--disable-preconnect");
        chromeOptions.addArguments("--disable-notifications");
        chromeOptions.addArguments("'--dns-prefetch-disable'");
        chromeOptions.setAcceptInsecureCerts(true);
        chromeOptions.addArguments("test-type");
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\src\\test\\resources\\driver\\chromedriver.exe");
        driver = new ChromeDriver(chromeOptions);

        //Configuration configuration = new Configuration();
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(IMPLICITYLY_WAIT, TimeUnit.SECONDS);
        //driver.get(configuration.getFirstBaseUrl());
        //driver.get(configuration.getSecondBaseUrl());

    }

    @After
    public void tearDown() {
        driver.quit();
        System.out.println("Test is finished. Driver is closed.");
    }

}