package base;

import com.beust.jcommander.internal.Lists;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.time.Duration;
import java.util.List;

public class TestBase {

    public void openPage(RemoteWebDriver driver, String url) {
        driver.navigate().to(url);
    }
    public void clickElement(WebElement element) {
        element.click();
        System.out.println(element + " element is clicked.");
    }

    public void wait(int seconds){
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void scrollToFindElement(RemoteWebDriver driver, WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
        System.out.println("Scrolled down to find " + element);
    }

    public void waitForElement(RemoteWebDriver driver, WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        System.out.println(element + " is exist.");
    }

    public void waitUntilElementThenClick(RemoteWebDriver driver, WebElement element) {
        waitForElement(driver, element);
        clickElement(element);
    }

    public void fillAreaWithGivenText(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);
    }

    public void switchCurrentPage(RemoteWebDriver driver, int i) {
        List<String> browserTabs = Lists.newArrayList(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(i));
        System.out.println("Switched to current page.");
    }

}
